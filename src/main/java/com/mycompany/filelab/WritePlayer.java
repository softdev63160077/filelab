/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NonWises
 */
public class WritePlayer {
    public static void main(String[] args) {
        Player o = new Player('O');
        Player x = new Player('X');
        o.Win();
        x.Loss();
        o.Draw();
        x.Draw();
        o.Win();
        x.Loss();
        FileOutputStream fos = null;
        try {
            File file = new File("players.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
}
